﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

using Everlight.TechTest.RunModels;

namespace Everlight.TechTest.Runner
{
	class Program
	{
		private static void ConfigureTest(IServiceCollection services)
		{
			services.AddTransient<IBinaryGateTreeContainer<BinaryGateNode>, BinaryGateTreeContainer>();
			services.AddTransient<IGuessBeforeRun<BinaryGateNode>, GuessBeforeRun>();
		}

		static void Main(string[] args)
		{
			ServiceCollection serviceCollection = new ServiceCollection();
			serviceCollection.AddLogging(configure => configure.AddConsole());
			serviceCollection.AddSingleton<TextWriter>(Console.Out);

			Program.ConfigureTest(serviceCollection);

			ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();

			Console.Write("Please type the number of level to create binary gate tree: ");
			string level = Console.ReadLine();

			int noOfLevel;
			bool isOK = Int32.TryParse(level, out noOfLevel);
			if (isOK == false)
			{
				Console.WriteLine("Failed to pass and exiting");
				return;
			}
			if (noOfLevel < 2)
			{
				Console.WriteLine("No need to continue and exiting");
				return;
			}

			IBinaryGateTreeContainer<BinaryGateNode> tree = serviceProvider.GetService<IBinaryGateTreeContainer<BinaryGateNode>>();
			IGuessBeforeRun<BinaryGateNode> guess = serviceProvider.GetService<IGuessBeforeRun<BinaryGateNode>>();

			int noOfBallContainer = tree.CreateTree(noOfLevel);
			IBinaryGateNode<BinaryGateNode> root = tree.Root;

			IPassBall notReceivedGuessed = guess.GuessWhoIsNotReceive(root);

			Console.Write("Press any key to pass balls");
			Console.ReadLine();

			for (int idx = 0; idx < (noOfBallContainer - 1); idx++)
			{
				Ball ball = new Ball() { Identifier = (idx + 1) };
				Console.WriteLine("Passing a ball, {0}th", (idx + 1));
				tree.PassBall(ball);
			}

			List<IPassBall> emptyContainers = tree.FindEmptyContainer();

			Console.WriteLine();
			if (emptyContainers == null || emptyContainers.Count < 1)
			{
				Console.WriteLine("Cannot find any empty container");
			}
			else
			{
				Console.WriteLine("Found {0} empty contaner(s), Identifier: {1}",
					emptyContainers.Count,
					String.Join(", ", emptyContainers.Select(cn => cn.Identifier).ToArray()));
			}
			Console.WriteLine("Guessed empty contaner, Identifier: {0}", notReceivedGuessed.Identifier);
			Console.WriteLine();

			Console.Write("Press any key and enter to exit");
			Console.ReadLine();
		}
	}
}
