﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Everlight.TechTest
{
	public interface IGuessBeforeRun<TNodeType> where TNodeType : class, IPassBall
	{
		IPassBall GuessWhoIsNotReceive(IBinaryGateNode<TNodeType> rootNode);
	}
}
