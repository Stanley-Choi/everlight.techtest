﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Everlight.TechTest
{
	public interface IBinaryGateTreeContainer<TNodeType> where TNodeType : class, IPassBall
	{
		/// <summary>
		/// Create tree by level
		/// </summary>
		/// <param name="level"></param>
		/// <returns>Supposed to return last no of container</returns>
		int CreateTree(int level);

		IBinaryGateNode<TNodeType> Root
		{
			get;
		}

		void PassBall(Ball ballToPass);

		List<IPassBall> FindEmptyContainer();
	}
}
