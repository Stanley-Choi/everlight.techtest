﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Microsoft.Extensions.Logging;

namespace Everlight.TechTest
{
	public interface IBinaryGateNode<TNodeType> : IPassBall where TNodeType : class, IPassBall
	{
		/// <summary>
		/// To call cascaded
		/// </summary>
		/// <param name="subLevel"></param>
		/// <param name="randForInitOpenDirection"></param>
		/// <param name="logger"></param>
		/// <param name="startIndex">informative to create Identifier</param>
		/// <returns>Evenutally returns no of ball containers by cascaded</returns>
		int InitializeSubTree(TextWriter logger, int subLevel, Random randForInitOpenDirection, int startIndex);

		TNodeType Left
		{
			get;
		}

		TNodeType Right
		{
			get;
		}


		NodeOpenDirections OpenDirection
		{
			get;
		}

		NodeTypes ? NodeType
		{
			get;
		}

		List<IPassBall> FindEmptyContainer();
	}
}
