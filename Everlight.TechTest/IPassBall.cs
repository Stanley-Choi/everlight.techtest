﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Everlight.TechTest
{
	public interface IPassBall
	{
		string Identifier
		{
			get;
		}

		Ball BallReceived
		{
			get;
		}

		void PassBall(Ball ballToPass);
	}
}
