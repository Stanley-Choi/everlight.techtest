﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using Microsoft.Extensions.Logging;

namespace Everlight.TechTest.RunModels
{
	public class BinaryGateNode : IBinaryGateNode<BinaryGateNode>
	{
		private TextWriter _logger = null;

		public BinaryGateNode()
		{
		}

		public BinaryGateNode(TextWriter logger)
		{
			this._logger = logger;
		}

		public int InitializeSubTree(TextWriter logger, int subLevel, Random randForInitOpenDirection, int startIndex)
		{
			this._logger = logger;
			int rand = randForInitOpenDirection.Next();
			if (rand > 1)
			{
				rand = rand % 2;
			}

			NodeOpenDirections init = (NodeOpenDirections)rand;
			this.OpenDirection = init;
			this.NodeType = (subLevel > 0) ? NodeTypes.Gate : NodeTypes.BallContainer;

			if (this.NodeType == NodeTypes.BallContainer)
			{
				this.Identifier = $"Container-{startIndex}";
				return 1;
			}

			this.Identifier = $"Gate-Level-{subLevel} [{startIndex}-To-{Math.Pow(2, subLevel)}]";

			if (this._logger != null)
			{
				this._logger.WriteLine("{0} Gate Init Opended. {1}", this.Identifier, this.OpenDirection);
				this._logger.Flush();
			}

			int nextSubLevel = subLevel - 1;
			int leftIndex = startIndex;
			int rightIndex = startIndex + (int)Math.Pow(2, nextSubLevel);

			this.Left = new BinaryGateNode();
			this.Right = new BinaryGateNode();
			int noOfLeftSide = this.Left.InitializeSubTree(logger, nextSubLevel, randForInitOpenDirection, leftIndex);
			int noOfRightSide = this.Right.InitializeSubTree(logger, nextSubLevel, randForInitOpenDirection, rightIndex);

			return (noOfLeftSide + noOfRightSide);
		}

		protected TextWriter Logger
		{
			get { return this._logger; }
		}

		public BinaryGateNode Left
		{
			get;
			protected set;
		}

		public BinaryGateNode Right
		{
			get;
			protected set;
		}

		public NodeOpenDirections OpenDirection
		{
			get;
			protected set;
		}

		public virtual void PassBall(Ball ball)
		{
			if (this.NodeType.HasValue == false)
			{
				throw (new ApplicationException("Node type is not initlialized - InitializeSubTree is not called ?"));
			}

			if (this.NodeType == NodeTypes.Gate)
			{
				BinaryGateNode toNode = null;
				if (this._logger != null)
				{
					this._logger.WriteLine("Ball, {0}, is going to {1}", ball.Identifier, this.OpenDirection);
					this._logger.Flush();
				}

				switch (this.OpenDirection)
				{
					case NodeOpenDirections.Left:
						toNode = this.Left;
						this.OpenDirection = NodeOpenDirections.Right;
						break;
					case NodeOpenDirections.Right:
						toNode = this.Right;
						this.OpenDirection = NodeOpenDirections.Left;
						break;
					default:
						break;
				}

				if (toNode != null)
				{
					toNode.PassBall(ball);
				}

				return;
			}

			this.BallReceived = ball;
		}

		public string Identifier
		{
			get;
			protected set;
		}

		public Ball BallReceived
		{
			get;
			protected set;
		}

		public NodeTypes ? NodeType
		{
			get;
			protected set;
		}

		public List<IPassBall> FindEmptyContainer()
		{
			if (this.NodeType.HasValue == false)
			{
				throw (new ApplicationException("Node type is not initlialized - InitializeSubTree is not called ?"));
			}

			List<IPassBall> rtn = null;

			switch (this.NodeType.Value)
			{
				case NodeTypes.BallContainer:
					if (this.BallReceived == null)
					{
						rtn = new List<IPassBall>();
						rtn.Add(this);
					}
					break;
				case NodeTypes.Gate:
					List<IPassBall> left = this.Left.FindEmptyContainer();
					List<IPassBall> right = this.Right.FindEmptyContainer();
					rtn = new List<IPassBall>();
					if (left != null && left.Count > 0)
					{
						rtn.AddRange(left);
					}
					if (right != null && right.Count > 0)
					{
						rtn.AddRange(right);
					}
					break;
			}

			return rtn;
		}
	}
}
