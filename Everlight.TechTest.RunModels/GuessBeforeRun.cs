﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Everlight.TechTest.RunModels
{
	public class GuessBeforeRun : IGuessBeforeRun<BinaryGateNode>
	{
		/// <summary>
		/// This is more logical reasoning but I'm thinking there will be better way to use logical operator or more likely combining logical operator.
		/// </summary>
		/// <typeparam name="TNodeType"></typeparam>
		/// <param name="rootNode"></param>
		/// <returns></returns>
		public IPassBall GuessWhoIsNotReceive(IBinaryGateNode<BinaryGateNode> rootNode)
		{
			if (rootNode == null)
			{
				return null;
			}

			IPassBall found = null;
			BinaryGateNode trace = null;

			switch (rootNode.OpenDirection)
			{
				case NodeOpenDirections.Left:
					trace = rootNode.Right;
					break;
				case NodeOpenDirections.Right:
					trace = rootNode.Left;
					break;
			}

			while (trace != null)
			{
				if (trace.NodeType == NodeTypes.BallContainer)
				{
					found = trace;
					break;
				}
				else
				{
					switch (trace.OpenDirection)
					{
						case NodeOpenDirections.Left:
							trace = trace.Right;
							break;
						case NodeOpenDirections.Right:
							trace = trace.Left;
							break;
					}
				}
			}

			return found;
		}
	}
}
