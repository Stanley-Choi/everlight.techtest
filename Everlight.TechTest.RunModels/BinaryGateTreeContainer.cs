﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Everlight.TechTest.RunModels
{
	public class BinaryGateTreeContainer : IBinaryGateTreeContainer<BinaryGateNode>
	{
		public const int Minimum_Tree_Level = 2;

		private TextWriter _logger = null;
		private BinaryGateNode _root = null;
		
		public BinaryGateTreeContainer()
		{
		}

		public BinaryGateTreeContainer(TextWriter logger)
		{
			this._logger = logger;
		}

		public IBinaryGateNode<BinaryGateNode> Root
		{
			get 
			{
				return this._root;// as IBinaryGateNode<BinaryGateNode>;
			}
		}

		public int CreateTree(int level)
		{
			if (level < BinaryGateTreeContainer.Minimum_Tree_Level)
			{
				throw (new ArgumentOutOfRangeException($"The level of tree should be {BinaryGateTreeContainer.Minimum_Tree_Level}, at least"));
			}

			Random rand = new Random();
			if (this._root == null)
			{
				this._root = new BinaryGateNode(this._logger);
			}
			
			return this._root.InitializeSubTree(this._logger, level, rand, 1);
		}

		public void PassBall(Ball ball)
		{
			if (this._root == null)
			{
				throw (new ApplicationException("Root node is not created yet"));
			}

			this._root.PassBall(ball);
		}

		public List<IPassBall> FindEmptyContainer()
		{
			if (this._root == null)
			{
				throw (new ApplicationException("Root node is not created yet"));
			}

			return this._root.FindEmptyContainer();
		}
	}
}
